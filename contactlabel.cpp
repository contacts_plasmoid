
#include "contactlabel.h"
#include <QLabel>
#include <QPainter>
#include <QGraphicsSceneEvent>
#include <QApplication>
#include <math.h>

static const double pi = 3.14159265359;


ContactLabel::ContactLabel(QGraphicsItem *parent)
    : QGraphicsWidget(parent), label(0), grabbed(false)
{
    setAcceptsHoverEvents(true);
    setFlag(ItemIgnoresParentOpacity);
}

ContactLabel::~ContactLabel()
{
}


void ContactLabel::setText(const QString &text)
{
    label = text;
}

void ContactLabel::setPen( const QPen &pen)
{
    mPen = pen;
}

void ContactLabel::setFont( const QFont &font)
{
    mFont = font;
}

void ContactLabel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
	   QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);
    //painter->drawText(rect, Qt::AlignCenter, *label);
    painter->setFont( mFont );
    painter->setPen( mPen );

    painter->drawText(rect(), Qt::AlignJustify, label);
}

/* TODO: cleanup this mess! */
/***************************************************************************/
static qreal lengthForPos(const QPointF &pos)
{
    return ::sqrt(pos.x() * pos.x() + pos.y() * pos.y());
}

static qreal angleForPos(const QPointF &pos)
{
    qreal angle = ::acos(pos.x() / lengthForPos(pos));
    if (pos.y() > 0)
        angle = pi * 2.0 - angle;
    return angle;
}
/*
void ContactLabel::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    return QGraphicsItem::mousePressEvent(event);
}
*/
/*
void ContactLabel::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (!grabbed)
        return QGraphicsItem::mouseReleaseEvent(event);

    grabbed = false;
    update();
}

*/
void ContactLabel::setHover(qreal value)
{
    hover = value;
    update();
}
