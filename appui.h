/*********************************************************************/
/* 								     */
/* Copyright (C)  2009  Adenilson Cavalcanti <>			     */
/* 								     */
/* This program is free software; you can redistribute it and/or     */
/* modify it under the terms of the GNU General Public License	     */
/* as published by the Free Software Foundation; either version 2    */
/* of the License, or (at your option) any later version.	     */
/*   								     */
/* This program is distributed in the hope that it will be useful,   */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of    */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     */
/* GNU General Public License for more details.			     */
/* 								     */
/* You should have received a copy of the GNU General Public License */
/* along with this program; if not, write to the Free Software	     */
/* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA     */
/* 02110-1301, USA.						     */
/*********************************************************************/

#ifndef APPUI_H
#define APPUI_H

#include <QGraphicsWidget>
#include <QGraphicsLinearLayout>

class OmniAnimation;
class OmniWidget;
class ContactButton;
class ContactWidget;

class QPropertyAnimation;
class QSequentialAnimationGroup;

class QtKineticListController;

class AppUI: public QGraphicsWidget/*, public KineticScrolling */{
    Q_OBJECT

public:
    AppUI(QGraphicsItem *parent = 0);

    virtual ~AppUI();

    void buildWorld( QString background );

private:
    void rotateAnimation();
    void resetRotateAnimation();

private:
    QRectF *previousDim;
    QGraphicsWidget *mGraphicsWidget;
    QGraphicsLinearLayout *mLayout;
    QGraphicsLinearLayout *mGraphicsLayout;
    OmniWidget *mOmniWidget;
    OmniAnimation *mOmniAnimation;
    ContactButton *mOmniButton;
    ContactButton *mHomeButton;
    QtKineticListController *controller;
    ContactWidget *mContactWidget;
    QPropertyAnimation *yRotationAnim;
    QPropertyAnimation *omniButtonAnim;
    QPropertyAnimation *homeButtonAnim;
    QPropertyAnimation *yWidgetRotationAnim;
    QSequentialAnimationGroup *seqAnimation;

public Q_SLOTS:
    void makeVisible();
    void hideWidget();
    void backwardAnimation();
    void forwardAnimation();


protected:
    QSizeF sizeHint( Qt::SizeHint which, const QSizeF & constraint = QSizeF() ) const;
    void resizeEvent( QGraphicsSceneResizeEvent * event );

protected:
    void animation();
};

#endif
