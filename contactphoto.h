#ifndef __CONTACT_PHOTO__
#define __CONTACT_PHOTO__

#include <QGraphicsWidget>
class QPixmap;
class QImage;

class ContactPhoto: public QGraphicsWidget
{
    Q_OBJECT
public:

  ContactPhoto(QGraphicsWidget *parent = 0, QGraphicsScene *scene = 0);

  void setPhoto(const QString &pixPath);
  void setShadow(const QString &pixPath);

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
	     QWidget *widget);

  QRectF boundingRect() const;

  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
  void setHover(qreal value);

  virtual ~ContactPhoto();

protected:

  void createShadow();

  bool grabbed;
  QPixmap *photo;
  QImage *source;
  QImage *shadow;
  QImage *result;
  qreal rotation;
  qreal newRotation;
  qreal scale;
  qreal newScale;
  qreal hover;
};


#endif
