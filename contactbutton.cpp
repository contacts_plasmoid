#include "contactbutton.h"

#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

ContactButton::ContactButton( const QString &pixmapPath, QGraphicsItem *parent )
    : QGraphicsWidget( parent )
{
    if ( mPixmap.load( pixmapPath ) )
        qDebug()<<"error loading path:"<<pixmapPath;
    resize(mPixmap.width(), mPixmap.height());
    setAcceptedMouseButtons( Qt::LeftButton );
    setOpacity(0.6);
}

ContactButton::ContactButton( ContactButton &button )
{
    setAcceptedMouseButtons( Qt::LeftButton );
    setGeometry( button.geometry() );
    setPos( 0, 0 );
    mPixmap = button.mPixmap;
    setFlag(ItemIgnoresParentOpacity);
}

void ContactButton::paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{

    painter->drawPixmap(QPoint( 0, 0 ), mPixmap);
}

void ContactButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    qDebug()<< "ContactButton::mousePressEvent = " << event->pos() <<"  "<< geometry();
    emit clicked();
    return QGraphicsItem::mousePressEvent(event);
}

QSizeF ContactButton::sizeHint( Qt::SizeHint which, const QSizeF & constraint ) const
{
    //Adjust it to work with pulser
/*     switch (which) {
         case Qt::MinimumSize:
         case Qt::PreferredSize:
             return mPixmap.size();
         case Qt::MaximumSize:
             return mPixmap.size();
         default:
             break;
     }
*/
     return QGraphicsWidget::sizeHint(which, constraint);
}
