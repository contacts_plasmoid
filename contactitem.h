/*********************************************************************/
/* 								     */
/* Copyright (C)  2009  Adenilson Cavalcanti <>			     */
/* 								     */
/* This program is free software; you can redistribute it and/or     */
/* modify it under the terms of the GNU General Public License	     */
/* as published by the Free Software Foundation; either version 2    */
/* of the License, or (at your option) any later version.	     */
/*   								     */
/* This program is distributed in the hope that it will be useful,   */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of    */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     */
/* GNU General Public License for more details.			     */
/* 								     */
/* You should have received a copy of the GNU General Public License */
/* along with this program; if not, write to the Free Software	     */
/* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA     */
/* 02110-1301, USA.						     */
/*********************************************************************/

#ifndef CONTACTITEM_H
#define CONTACTITEM_H

#include <QGraphicsWidget>
#include "contactphoto.h"
#include "contactlabel.h"

#include "qgraphicslistview.h"

class QGraphicsLinearLayout;
class QGraphicsGridLayout;
class QSvgRenderer;
class QPixmap;

class ContactItem: public QtGraphicsListViewItem
{
Q_OBJECT

public:

    ContactItem( int itemIndex, QtGraphicsListView *mainView = 0);

    virtual ~ContactItem();

    QSizeF sizeHint(int index, const QStyleOptionViewItemV4 *option, Qt::SizeHint which, const QSizeF &constraint = QSizeF()) const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
            QWidget *widget);

    bool loadBackground( char *path );

    void setOpacityChild( bool flag );

protected:
    QGraphicsLinearLayout *widget;
    QGraphicsGridLayout *layout;
    ContactPhoto *photo;
    ContactLabel *name;
    ContactLabel *email;
    ContactLabel *phone;
    QSvgRenderer *svgBackground;
    QPixmap *background;

};

#endif
