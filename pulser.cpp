#include <QGraphicsWidget>
#include <QEvent>
#include <QAbstractAnimation>
#include <QParallelAnimationGroup>
#include <QDebug>
#include <QPropertyAnimation>
#include "pulser.h"
#include "proxyopacitysetter.h"

PulseBase::~PulseBase()
{
     delete animation;
     delete pulseGeometry;
}

void PulseBase::start()
{
    under->setOpacity( 1 );
    animation->start();
}

QRectF PulseBase::geometry()
{
    return under->geometry();
}


void PulseBase::updateGeometry(QRectF updated, qreal zCoordinate, qreal scale)
{
    zvalue = zCoordinate;
    --zvalue;
    under->setGeometry( updated );
    under->setPos( 0, 0 );
    under->setOpacity( 0 );
    under->setZValue( zvalue );

    /* TODO: move this to a function */
    QRectF initial( under->geometry() );
    qreal W = initial.width() * scale * 0.33;
    qreal H = initial.height() * scale * 0.33;
    QRectF end( initial.x() - W, initial.y() -  H, initial.width() * scale,
        initial.height() * scale );
    anim2->setEndValue(end);

    anim2->setEndValue(end);


}

void PulseBase::resetPulser()
{
    under->setGeometry( *pulseGeometry );
    under->setOpacity( 0 );
    under->setZValue( zvalue );
    under->setScale( mscale );
}

void PulseBase::createAnimation( qreal duration, qreal scale,
                            ProxyOpacitySetter *setter )
{

    pulseGeometry = new QRectF( under->geometry() );
    QParallelAnimationGroup *group = new QParallelAnimationGroup(this);
    anim1 = new QPropertyAnimation(under, "opacity");
    anim1->setDuration(duration);
    anim1->setEndValue(0);
    group->addAnimation(anim1);

    /* TODO: move this to a function */
    anim2 = new QPropertyAnimation(under, "geometry");
    anim2->setDuration(duration);
    QRectF initial( under->geometry() );
    qreal W = initial.width() * scale * 0.33;
    qreal H = initial.height() * scale * 0.33;
    QRectF end( initial.x() - W, initial.y() -  H, initial.width() * scale,
        initial.height() * scale );
    anim2->setEndValue(end);
    group->addAnimation(anim2);

    anim3 = new QPropertyAnimation(under, "scale");
    anim3->setDuration( duration );
    anim3->setEndValue( scale );
    group->addAnimation( anim3 );

    animation = group;

    connect( animation, SIGNAL( finished() ), this, SLOT( resetPulser() ) );
}


//XXX: if someday exists a C++ compiler that works, use it!
// template <typename TYPE>
// Pulser<TYPE>::Pulser( TYPE *target, qreal duration, qreal scale,
//                       ProxyOpacitySetter *setter )
// {

//     TYPE *under = new TYPE( *target );
//     under->setZValue( target->zValue() - 1 );
//     under->setParentItem( target );
//     QParallelAnimationGroup *group = new QParallelAnimationGroup(this);
//     QPropertyAnimation *anim1 = new QPropertyAnimation(under, "opacity");
//     anim1->setDuration(duration);
//     anim1->setEndValue(0);
//     group->addAnimation(anim1);

//     QPropertyAnimation *anim2 = new QPropertyAnimation(under, "geometry");
//     anim2->setDuration(duration);
//     QRectF end;
//     if ( under->geometry().x() != 0 )
//         end = QRectF( under->geometry().x() - under->geometry().width() * scale,
//                 under->geometry().y() - under->geometry().height() * scale,
//                 under->geometry().width() * scale,
//                 under->geometry().height() * scale );
//     else
//         end = QRectF( under->geometry().width() * -.25,
//                       under->geometry().height() * -.25,
//                 under->geometry().width() * scale,
//                 under->geometry().height() * scale );


//     anim2->setEndValue(end);
//     group->addAnimation(anim2);
//     animation = group;

// }
