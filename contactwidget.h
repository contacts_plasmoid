#ifndef CONTACTWIDGET_H
#define CONTACTWIDGET_H

#include <QGraphicsWidget>

class ContactPhoto;
class ContactLabel;
class QGraphicsGridLayout;

class ContactWidget : public QGraphicsWidget
{
    public:
        ContactWidget( QGraphicsItem * parent = 0 );
        ~ContactWidget();

    private:
        ContactPhoto *mPhoto;
        ContactLabel *label;
        QGraphicsGridLayout *layout;
};

#endif
