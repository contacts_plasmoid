#include "contactframe.h"

#include <QPainter>

ContactFrame::ContactFrame( QGraphicsItem *parent )
    : QGraphicsWidget( parent )
{
}

void ContactFrame::setPen( const QPen &pen)
{
    mPen = pen;
}

void ContactFrame::paint( QPainter *painter, const QStyleOptionGraphicsItem* option, QWidget *widget)
{
    Q_UNUSED( option );
    Q_UNUSED( widget );
    painter->setPen(mPen);
    painter->drawRoundedRect(rect(), 19, 19);
}
