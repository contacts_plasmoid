#include "contactitem.h"

#include <qlistmodelinterface.h>
#include <QGraphicsLinearLayout>
#include <QGraphicsGridLayout>
#include <QPainter>
#include <QSvgRenderer>

#include <QtGui>

#include <QDebug>

#ifndef STANDALONE
#include <kstandarddirs.h>
#endif

/* TODO:
 *  - check for memory leaks in scroll list
 *
 */

ContactItem::~ContactItem()
{
    qDebug() << "contactitem destructor...";
}

ContactItem::ContactItem( int itemIndex, QtGraphicsListView *mainView) :QtGraphicsListViewItem( itemIndex, mainView ),
                                                     svgBackground( 0 ),
                                                     background( 0 )
{
    QRectF tmp;

    setAcceptHoverEvents(true);
    ///////////////////////////////////////////////////////////////////////////
    // if (scene)                                                            //
    //     tmp = QRectF(10, 10, scene->width()/2, scene->height()/2);        //
    // else                                                                  //
    ///////////////////////////////////////////////////////////////////////////
    tmp = QRectF(0, 0, 250, 170);

    setGeometry(tmp);

    widget = new QGraphicsLinearLayout( Qt::Vertical, this );
    layout = new QGraphicsGridLayout( widget);
    layout->setVerticalSpacing(0.0);

#ifndef STANDALONE
    QString iconPath( KStandardDirs::installPath("data") + "plasma-contacts/" );
#else
    QString iconPath( "images/" );
#endif
    photo = new ContactPhoto();
    photo->setPhoto( iconPath + "wallace-ld.jpg");
    photo->setShadow( iconPath + "shadow.png" );

    const QHash<int, QVariant> hash = view()->model()->data(itemIndex, QList<int>() << 1);
    qDebug()<<index();

    QString contactName(hash[1].toString());
    name = new ContactLabel(this);
    name->setText(contactName);

    QPen pen;
    pen.setColor(QColor(130, 130, 130)); //dark black
    name->setPen( pen );

    QFont font("Helvetica [Cronyx]", 19);
    name->setFont( font );

    phone = new ContactLabel(this);
    phone->setText( "11111 - 11111");
    font.setFamily("Helvetica [Cronyx]");
    font.setPointSize(10);
    font.setItalic(false);
    phone->setFont( font );
    phone->setPen( pen );

    email = new ContactLabel(this);
    email->setText( "email@email.com");
    font.setFamily("Helvetica [Cronyx]");
    font.setPointSize(10);
    font.setItalic(true);
    email->setFont( font );
    pen.setColor(QColor(Qt::blue));
    email->setPen( pen );

    layout->addItem( photo, 0, 0);
    layout->addItem( name, 0, 1, Qt::AlignHCenter );
    layout->addItem( phone, 1, 1, Qt::AlignBottom );
    layout->addItem( email, 2, 1, Qt::AlignTop );

    widget->addItem( layout );

    setLayout( widget );
    //TODO: fix svg loading
    //loadBackground( "./background.svg" );
    setOpacity(0.25);

}

bool ContactItem::loadBackground( char *path )
{
    QString spath( path );
    if ( svgBackground )
        delete svgBackground;
    svgBackground = new QSvgRenderer( spath );
    QImage image;
    QPainter painter( &image );

    svgBackground->render( &painter, geometry() );
    if ( background )
        delete background;
    background = new QPixmap( QPixmap::fromImage( image ) );
}


QSizeF ContactItem::sizeHint(int index, const QStyleOptionViewItemV4 *option, Qt::SizeHint which, const QSizeF &constraint) const
{
    switch (which) {
        case Qt::MinimumSize:
        case Qt::PreferredSize: {
            QSizeF widgetSize = QGraphicsWidget::sizeHint(which, constraint);
            return QSizeF(widgetSize.width(), 150);
        }
        case Qt::MaximumSize:
            return QSizeF(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX);
        default:
            break;
    }
    return QtGraphicsListViewItem::sizeHint(index, option, which, constraint);
}

void ContactItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
	   QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);

    painter->setBrush(Qt::white);

    QPen pen;
    pen.setCapStyle( Qt::FlatCap );
    pen.setJoinStyle( Qt::RoundJoin );
    pen.setWidth(3);
    pen.setColor(Qt::lightGray);
    painter->setPen(pen);

    painter->drawRoundedRect(QRectF(rect().x(), rect().y(), rect().width(), rect().height() - 20), 20.0, 15.0);

    /*******************************************************/
    /* painter->drawPixmap( QPoint( 5, 5 ), *background ); */
    /*******************************************************/
}

void ContactItem::setOpacityChild( bool flag )
{
    if ( flag ) {
        photo->setFlag( ItemIgnoresParentOpacity );
        name->setFlag( ItemIgnoresParentOpacity );
        phone->setFlag( ItemIgnoresParentOpacity );
        email->setFlag( ItemIgnoresParentOpacity );
    } else {
        photo->setFlag( ItemIgnoresParentOpacity, false );
        photo->setOpacity( 1.0 );
        name->setFlag( ItemIgnoresParentOpacity, false );
        photo->setOpacity( 1.0 );
        phone->setFlag( ItemIgnoresParentOpacity, false );
        phone->setOpacity( 1.0 );
        email->setFlag( ItemIgnoresParentOpacity, false );
        email->setOpacity( 1.0 );
    }

}
