/***********************************************************************/
/* omniwidget.h 						       */
/* 								       */
/* Copyright(C) 2009 Igor Trindade Oliveira <igor.oliveira@indt.org.br>*/
/* Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>*/
/* 								       */
/* This library is free software; you can redistribute it and/or       */
/* modify it under the terms of the GNU Lesser General Public	       */
/* License as published by the Free Software Foundation; either	       */
/* version 2.1 of the License, or (at your option) any later version.  */
/*   								       */
/* This library is distributed in the hope that it will be useful,     */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   */
/* Lesser General Public License for more details.		       */
/*  								       */
/* You should have received a copy of the GNU Lesser General Public    */
/* License along with this library; if not, write to the Free Software */
/* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       */
/* 02110-1301  USA						       */
/***********************************************************************/
#ifndef OMNIWIDGET_H
#define OMNIWIDGET_H

#include "omniitem.h"
#include "pulser.h"

#include <QGraphicsWidget>
#include <QGraphicsProxyWidget>
#include <QGraphicsLinearLayout>
class PulseAnimation;

class OmniWidget : public QGraphicsWidget
{
    Q_OBJECT

    public:
        OmniWidget(QGraphicsItem *parent = 0);
        ~OmniWidget();

        void paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0 );

        void createItems();
        void setImagePath( const QString &path);

    Q_SIGNALS:
        void addItemClicked();
        void editItemClicked();
        void deleteItemClicked();

    protected:
	QVariant itemChange(GraphicsItemChange change, const QVariant &value);
	void updateItemPulser();

    private:
        OmniItem *addItem, *editItem, *deleteItem;
        QGraphicsLinearLayout *mLayout;
        QString mImagePath;
	PulseAnimation *anim[3];
	Pulser<OmniItem> *pulser1, *pulser2, *pulser3;
};

#endif
