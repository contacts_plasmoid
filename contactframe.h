#ifndef CONTACT_FRAME_H
#define CONTACT_FRAME_H

#include <QGraphicsWidget>

#include <QPen>

class ContactFrame : public QGraphicsWidget
{
    public:
        ContactFrame( QGraphicsItem *parent = 0 );
        void setPen( const QPen &pen );

        void paint ( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0 );

    private:
        QPen mPen;
};

#endif
