/////////////////////////////////////////////////////////////////////////
// pulseanimation.cpp                                                  //
//                                                                     //
// Copyright(C) 2009 Igor Trindade Oliveira <igor.oliveira@indt.org.br>//
// Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>//
//                                                                     //
// This library is free software; you can redistribute it and/or       //
// modify it under the terms of the GNU Lesser General Public          //
// License as published by the Free Software Foundation; either        //
// version 2.1 of the License, or (at your option) any later version.  //
//                                                                     //
// This library is distributed in the hope that it will be useful,     //
// but WITHOUT ANY WARRANTY; without even the implied warranty of      //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   //
// Lesser General Public License for more details.                     //
//                                                                     //
// You should have received a copy of the GNU Lesser General Public    //
// License along with this library; if not, write to the Free Software //
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       //
// 02110-1301  USA                                                     //
/////////////////////////////////////////////////////////////////////////
#include "pulseanimation.h"

PulseAnimation::PulseAnimation(QGraphicsWidget *target, QObject *parent)
    : QObject(parent), m_inside(false)
{
    target->installEventFilter(this);
    target->setAcceptHoverEvents(true);

    qDebug()<<"here3";

    QSequentialAnimationGroup *group = new QSequentialAnimationGroup(this);
    QPropertyAnimation *anim1 = new QPropertyAnimation(target, "yScale");
    QVariant originalValue = target->property("yScale");
    // ### duration etc. should be configurable
    anim1->setDuration(50);
    anim1->setEndValue(1.40);
    group->addAnimation(anim1);
    QPropertyAnimation *anim2 = new QPropertyAnimation(target, "yScale");
    anim2->setDuration(1250);
    anim2->setEndValue(originalValue);
    anim2->setEasingCurve(QEasingCurve::OutElastic);
    group->addAnimation(anim2);
    //QObject::connect(group, SIGNAL(finished()), this, SLOT(onAnimationFinished()));
    m_animation = group;
}

bool PulseAnimation::eventFilter(QObject *o, QEvent *e)
{
    QGraphicsWidget *widget = qobject_cast<QGraphicsWidget*>(o);
    qDebug()<<"here 2";
    if (!widget)
        return false;
    if (e->type() == QEvent::GraphicsSceneHoverEnter) {
        m_inside = true;
        m_animation->start();
    } else if (e->type() == QEvent::GraphicsSceneHoverLeave) {
        m_inside = false;
    }
    return false;
}
