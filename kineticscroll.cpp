#include "kineticscroll.h"

#include <QTime>
#include <QDebug>
#include <QEasingCurve>

unsigned int KineticScrollingPrivate::currentTimeInSecs()
{
    QTime t = QTime::currentTime();
    return (t.hour() * 3600000 + t.minute() * 60000 + t.second() * 1000 + t.msec());
}

KineticScrolling::KineticScrolling()
{
    d = new KineticScrollingPrivate;
}

qreal KineticScrolling::movement()
{
    return d->movement;
}

qreal KineticScrolling::duration()
{
    return d->timeDelta;
}

void KineticScrolling::mousePressEvent( QGraphicsSceneMouseEvent *event )
{
    d->timeStamp = d->currentTimeInSecs();
    d->scrollVelocity = 0;
    d->movement = 0;

    qDebug()<< "MousePressEvent";
}

void KineticScrolling::mouseMoveEvent( QGraphicsSceneMouseEvent *event )
{
    d->movement = event->lastPos().y() - event->pos().y();

    d->timeDelta = d->currentTimeInSecs() - d->timeStamp;
    qDebug()<<"MouseMoveEvent";
    qDebug()<<"movement:"<<d->movement;
}

void KineticScrolling::mouseReleaseEvent( QGraphicsSceneMouseEvent *event )
{
    qDebug()<<"MouseReleaseEvent";
    qDebug()<<"movement:"<<d->movement;
}
