/////////////////////////////////////////////////////////////////////////
// contacts.cpp                                                        //
//                                                                     //
// Copyright(C) 2009 Igor Trindade Oliveira <igor.oliveira@indt.org.br>//
// Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>//
//                                                                     //
// This library is free software; you can redistribute it and/or       //
// modify it under the terms of the GNU Lesser General Public          //
// License as published by the Free Software Foundation; either        //
// version 2.1 of the License, or (at your option) any later version.  //
//                                                                     //
// This library is distributed in the hope that it will be useful,     //
// but WITHOUT ANY WARRANTY; without even the implied warranty of      //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   //
// Lesser General Public License for more details.                     //
//                                                                     //
// You should have received a copy of the GNU Lesser General Public    //
// License along with this library; if not, write to the Free Software //
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       //
// 02110-1301  USA                                                     //
/////////////////////////////////////////////////////////////////////////
#include "contacts.h"
#include "contactbutton.h"

#include "experimental/qkineticlistcontroller.h"
#include "contactsview.h"
#include "contactsmodel.h"
#include "contactitem.h"
#include "contactwidget.h"

#include <kstandarddirs.h>


// TODO:
// - improve name, email and phone alignment in contactitem
// - test it with plasma netbook
// - fix the kinetic list (or write a new one)
// - research how to create text shadows
// - fix animation memory leak
// - provide an interface for contact data retrieval (i.e. stub)

K_EXPORT_PLASMA_APPLET(contactsPlasmoid, ContactsPlasmoid)

const int PLASMOID_WIDTH_BORD = 22;

ContactsPlasmoid::ContactsPlasmoid(QObject *parent, const QVariantList &args)
    : Plasma::Applet( parent, args ), previousDim( 0 ), mOmnianimation( 0 ),
      mLayout( 0 ), mOmniButton( 0 )
{
    setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);
}

ContactsPlasmoid::~ContactsPlasmoid()
{
    delete previousDim;
    delete mOmnianimation;
    delete pulser;
}

void ContactsPlasmoid::init()
{
    /* TODO: catch allocation exceptions */
    mLayout = new QGraphicsLinearLayout( Qt::Vertical, this );
    resize(400, 600);
    setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred );
    mLayout->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred );
    mGraphicsWidget = new QGraphicsWidget(this);
    mGraphicsWidget->resize( 400, 600 );
    mGraphicsLayout = new QGraphicsLinearLayout(Qt::Vertical, mGraphicsWidget);

    controller = new QtKineticListController(this);
    controller->setView(new ContactsView);
    controller->setModel(new ContactsModel(controller));
    controller->setOvershootEnabled(true);
    controller->view()->setFlag(QGraphicsItem::ItemClipsChildrenToShape, true);

    mGraphicsLayout->addItem( controller->view() );
    mLayout->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    mLayout->addItem( mGraphicsWidget );

    mOmniWidget = new OmniWidget( this );
    mOmniWidget->setImagePath( KStandardDirs::installPath("data") + "plasma-contacts/" );
    mOmniWidget->createItems();
    mOmniWidget->hide();

    mOmniButton = new ContactButton( KStandardDirs::installPath("data") + "plasma-contacts/omnibutton.png", this );
    connect( mOmniButton, SIGNAL( clicked() ), this, SLOT( makeVisible() ) );
    connect( mOmniWidget, SIGNAL( addItemClicked() ), this, SLOT( hideWidget() ) );
    mOmniButton->setPos(geometry().width() -
                        mOmniButton->geometry().width() - PLASMOID_WIDTH_BORD,
                        geometry().height() -
                        mOmniButton->geometry().height() - PLASMOID_WIDTH_BORD);

    mHomeButton = new ContactButton( KStandardDirs::installPath( "data" ) + "plasma-contacts/homebutton.png", this );
    mHomeButton->setPos( geometry().width() +
            mHomeButton->geometry().width() + PLASMOID_WIDTH_BORD,
            geometry().height() +
            mHomeButton->geometry().height() + PLASMOID_WIDTH_BORD );

    mContactWidget = new ContactWidget( this );
    mContactWidget->resize( 400, 600 );

    pulser = new Pulser<ContactButton> ( mOmniButton );
    connect( mOmniButton, SIGNAL( clicked() ), pulser, SLOT( start() ) );

    previousDim = new QRectF( geometry() );
    mOmnianimation = new OmniAnimation(this, mOmniWidget, mOmniButton);

    animation();
    rotateAnimation();
}

void ContactsPlasmoid::animation()
{
    qreal widthLayoutMargin, xWidgetMargin, yWidgetMargin;
    getContentsMargins(0, 0, &xWidgetMargin, &yWidgetMargin);
    mLayout->getContentsMargins(0, 0, &widthLayoutMargin, 0);
    mOmnianimation->create( &size(),  widthLayoutMargin, xWidgetMargin, yWidgetMargin );
}

void ContactsPlasmoid::rotateAnimation( )
{
    QGraphicsRotation *r = new QGraphicsRotation( mGraphicsWidget );
    r->setAxis( QVector3D(  0, 1, 0 ) );
    qreal x( mGraphicsWidget->size().width( )/2 );
    qreal y( mGraphicsWidget->size().height()/2 );
    r->setOrigin( QVector3D( x, y, 0.0 ) );
    QList<QGraphicsTransform *> r1;
    r1.append( r );
    mGraphicsWidget->setTransformations( r1 );

    yRotationAnim = new QPropertyAnimation( r, "angle", mGraphicsWidget );
    yRotationAnim->setStartValue( 0.0 );
    yRotationAnim->setEndValue( 90.0 );
    yRotationAnim->setDuration( 500 );

    omniButtonAnim = new QPropertyAnimation( mOmniButton, "geometry", this );
    QRectF tmpGeometry( mOmniButton->geometry() );
    omniButtonAnim->setStartValue( tmpGeometry );
    omniButtonAnim->setEndValue( QRectF( tmpGeometry.x() + tmpGeometry.width() + PLASMOID_WIDTH_BORD,
                tmpGeometry.y() + tmpGeometry.height() + PLASMOID_WIDTH_BORD,
                tmpGeometry.width(), tmpGeometry.height() ) );

    seqAnimation = new QSequentialAnimationGroup( this );
    seqAnimation->addAnimation( omniButtonAnim );
    seqAnimation->addAnimation( yRotationAnim );

    QGraphicsRotation *r2 = new QGraphicsRotation( mContactWidget );
    r2->setAxis( QVector3D(  0, 1, 0 ) );
    x = mContactWidget->size().width( )/2;
    y = mContactWidget->size().height( )/2;
    r2->setOrigin( QVector3D(  x, y, 0 ) );
    /* XXX: should be -90.0 */
    r2->setAngle( -101.0 );
    QList<QGraphicsTransform *> r3;
    r3.append( r2 );
    mContactWidget->setTransformations( r3 );

    yWidgetRotationAnim = new QPropertyAnimation( r2, "angle", mContactWidget );
    /* XXX: should be -90.0 */
    yWidgetRotationAnim->setStartValue( -101.0 );
    yWidgetRotationAnim->setEndValue( 0 );
    yWidgetRotationAnim->setDuration( 500 );

    homeButtonAnim = new QPropertyAnimation( mHomeButton, "geometry", this );
    tmpGeometry = mHomeButton->geometry();
    homeButtonAnim->setStartValue( tmpGeometry );
    homeButtonAnim->setEndValue( QRectF( geometry().width() - tmpGeometry.width() - PLASMOID_WIDTH_BORD,
                geometry().height() - tmpGeometry.height() - PLASMOID_WIDTH_BORD,
                tmpGeometry.width(), tmpGeometry.height() ) );

    seqAnimation->addAnimation( yWidgetRotationAnim );
    seqAnimation->addAnimation( homeButtonAnim );

    connect( controller->view(), SIGNAL(  clicked( ) ), this, SLOT(  forwardAnimation( ) ) );
    connect( mHomeButton, SIGNAL( clicked() ), this, SLOT( backwardAnimation() ) );
}

void ContactsPlasmoid::resetRotateAnimation()
{
    QRectF tmpGeometry( mOmniButton->geometry() );
    omniButtonAnim->setStartValue( tmpGeometry );
    omniButtonAnim->setEndValue( QRectF( tmpGeometry.x() + tmpGeometry.width() + PLASMOID_WIDTH_BORD,
                tmpGeometry.y() + tmpGeometry.height() + PLASMOID_WIDTH_BORD,
                tmpGeometry.width(), tmpGeometry.height() ) );

    tmpGeometry = mHomeButton->geometry();
    homeButtonAnim->setStartValue( tmpGeometry );
    homeButtonAnim->setEndValue( QRectF( geometry().width() - tmpGeometry.width() - PLASMOID_WIDTH_BORD,
                geometry().height() - tmpGeometry.height() - PLASMOID_WIDTH_BORD,
                tmpGeometry.width(), tmpGeometry.height() ) );
}

void ContactsPlasmoid::makeVisible()
{
   mOmniWidget->setVisible(true);
   QtListModelInterface *model = controller->model();
   QtGraphicsListView *view = controller->view();
   for ( int i = 0; i < model->count(); ++i ) {
       ContactItem *tmp = dynamic_cast<ContactItem*>(  view->itemForIndex( i ) );
       if ( tmp )
           tmp->setOpacityChild( false );

   }
}

void ContactsPlasmoid::hideWidget()
{
   QtListModelInterface *model = controller->model();
   QtGraphicsListView *view = controller->view();
   for ( int i = 0; i < model->count(); ++i ) {
       ContactItem *tmp = dynamic_cast<ContactItem*>(  view->itemForIndex( i ) );
       if ( tmp )
           tmp->setOpacityChild( true );

   }
}

QSizeF ContactsPlasmoid::sizeHint( Qt::SizeHint which, const QSizeF & constraint ) const
{
    switch (which) {
        case Qt::MinimumSize:
            return QSizeF(240,320);
        case Qt::PreferredSize:
            qDebug() << size();
            return QSizeF(geometry().width(), geometry().height());
        case Qt::MaximumSize:
            return QSizeF(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX);
        default:
            break;
    }

    return QGraphicsWidget::sizeHint(which, constraint);
}

void ContactsPlasmoid::resizeEvent( QGraphicsSceneResizeEvent * event )
{
    if( mOmniButton && mLayout && mOmnianimation ) {
        qreal widthLayoutMargin, xWidgetMargin, yWidgetMargin;
        getContentsMargins(0, 0, &xWidgetMargin, &yWidgetMargin);

        if( mOmniButton->isClipped() ) {
            mOmniButton->setPos(size().width() -
                    mOmniButton->size().width() - PLASMOID_WIDTH_BORD,
                    size().height() -
                    mOmniButton->size().height() - PLASMOID_WIDTH_BORD);

            mHomeButton->setPos( size().width() +
                    mHomeButton->size().width() + PLASMOID_WIDTH_BORD,
                    size().height() +
                    mHomeButton->size().height() + PLASMOID_WIDTH_BORD );
        } else {
            mOmniButton->setPos( size( ).width( ) +
                    mOmniButton->size( ).width( ) + PLASMOID_WIDTH_BORD,
                    size( ).height( ) +
                    mOmniButton->size( ).height( ) + PLASMOID_WIDTH_BORD);

            mHomeButton->setPos( size().width() -
                    mHomeButton->size().width() - PLASMOID_WIDTH_BORD,
                    size().height() -
                    mHomeButton->size().height() - PLASMOID_WIDTH_BORD );
        }

        mLayout->getContentsMargins(0, 0, &widthLayoutMargin, 0);
        mOmnianimation->reset( &size(), widthLayoutMargin, xWidgetMargin, yWidgetMargin );
        resetRotateAnimation();
    }
    Plasma::Applet::resizeEvent(event);
}

void ContactsPlasmoid::backwardAnimation()
{
    seqAnimation->setDirection( QAbstractAnimation::Backward );
    seqAnimation->start();
}

void ContactsPlasmoid::forwardAnimation()
{
    seqAnimation->setDirection( QAbstractAnimation::Forward );
    seqAnimation->start();
}
