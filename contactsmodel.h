#ifndef CONTACTS_MODEL_H
#define CONTACTS_MODEL_H

#include "qlistmodelinterface.h"

class ContactsModel : public QtListModelInterface
{
    public:
        ContactsModel( QObject *parent = 0);

        int count() const;
        QHash<int,QVariant> data(int index, const QList<int> &roles) const;

    private:
        QList<QString> names;
};

#endif
