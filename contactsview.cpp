#include "contactsview.h"
#include "contactitem.h"
#include "pulseanimation.h"

#include "qgraphicslistview.h"
#include "qlistmodelinterface.h"



ContactsView::ContactsView( QGraphicsWidget *parent)
    : QtGraphicsListView(Qt::Vertical, parent, 0)
{
    setItemCreator(new QtGraphicsListViewItemCreator<ContactItem>());
}

void ContactsView::initStyleOption(QStyleOptionViewItemV4 *option) const
{
        QtGraphicsListView::initStyleOption(option);
//          option->features |= QStyleOptionViewItemV2::WrapText;
}

bool ContactsView::event(QEvent *event)
{
    if( event->type() == QEvent::GraphicsSceneResize) {
        for(int i = 0; i < model()->count(); i++) {
           QtGraphicsListViewItem *item =  itemForIndex(i);
           if( !item )
               continue;
           item->resize(geometry().width(),150);
           itemGeometryChanged( item );
        }
    }
    return QtGraphicsListView::event(event);
}

void ContactsView::mousePressEvent( QGraphicsSceneMouseEvent *event)
{
    emit clicked( );
    qDebug()<<"ContactsView::clicked()";
    return QtGraphicsListView::mousePressEvent( event);
}
