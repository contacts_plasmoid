#ifndef KINETICLAYOUT_H
#define KINETICLAYOUT_H

#include <QGraphicsWidget>
#include <QPropertyAnimation>
#include <QGraphicsSceneMouseEvent>
#include "kineticscroll.h"

class KineticView : public QGraphicsWidget, KineticScrolling
{
    public:
        KineticView(QGraphicsWidget *parent = 0);
        ~KineticView();

        void insertItem( QGraphicsWidget *item);
        QGraphicsItem * itemAt( int i );

    private:
        void scrollingAnimation(unsigned int duration, qreal startValue, qreal endValue);

    private:
        QPropertyAnimation *scroll;
        QList<QGraphicsWidget *> itemList;

    protected:
        virtual bool event ( QEvent * event );
};

#endif
