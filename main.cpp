#include <QtGui>
#include "appui.h"

#include "kineticview.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QGraphicsScene scene;
    scene.setSceneRect(0, 0, 600, 600);

    AppUI *ui = new AppUI;

    scene.addItem( ui );

    ui->buildWorld("background:red");


    QGraphicsView view(&scene);
    view.setRenderHints( view.renderHints() | QPainter::Antialiasing |
            QPainter::SmoothPixmapTransform | QPainter::TextAntialiasing );
    /* view drag */
    view.setDragMode(QGraphicsView::ScrollHandDrag);
    view.show();
    return app.exec();
}
