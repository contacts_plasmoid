#ifndef KINETICSCROLLINGPRIVATE_H
#define KINETICSCROLLINGPRIVATE_H

class KineticScrollingPrivate
{
    public:

        unsigned int currentTimeInSecs();

        unsigned int timeStamp;
        unsigned int timeDelta;
        qreal scrollVelocity;
        qreal movement;
};

#endif
