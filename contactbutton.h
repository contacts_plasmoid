#ifndef ContactButton_H
#define ContactButton_H

#include <QGraphicsWidget>
#include <QPixmap>

class ContactButton : public QGraphicsWidget
{
    Q_OBJECT

    public:
        ContactButton( const QString &pixmapPath, QGraphicsItem *parent = 0 );
        ContactButton( ContactButton &button );

        void paint ( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0 );

        void mousePressEvent(QGraphicsSceneMouseEvent *event);

    protected:
        QSizeF  sizeHint ( Qt::SizeHint which, const QSizeF & constraint = QSizeF() ) const;

    private:
        QPixmap mPixmap;

    Q_SIGNALS:
        void clicked();
};

#endif
