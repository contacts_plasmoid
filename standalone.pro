QMAKE_CXXFLAGS += -g -Wall
INCLUDEPATH += itemviews-ng/src itemviews-ng/src/experimental
LIBRARYPATH += itemviews-ng/lib
LIBS += -L$$LIBRARYPATH -litemviews-ng
QT           += svg xml gui
CONFIG -= app_bundle
DEFINES += Q_ITEMVIEWSNG_EXPORT=
DEFINES += STANDALONE
SOURCES += main.cpp appui.cpp omniwidget.cpp omnianimation.cpp omniitem.cpp contactbutton.cpp contactsmodel.cpp contactsview.cpp contactphoto.cpp contactlabel.cpp contactitem.cpp pulser.cpp contactwidget.cpp contactframe.cpp

HEADERS += appui.h omniwidget.h omnianimation.h omniitem.h contactbutton.h contactsmodel.h contactsview.h contactphoto.h contactlabel.h contactitem.h pulser.h contactwidget.h contactframe.h
