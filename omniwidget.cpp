/////////////////////////////////////////////////////////////////////////
// omniwidget.cpp                                                      //
//                                                                     //
// Copyright(C) 2009 Igor Trindade Oliveira <igor.oliveira@indt.org.br>//
// Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>//
//                                                                     //
// This library is free software; you can redistribute it and/or       //
// modify it under the terms of the GNU Lesser General Public          //
// License as published by the Free Software Foundation; either        //
// version 2.1 of the License, or (at your option) any later version.  //
//                                                                     //
// This library is distributed in the hope that it will be useful,     //
// but WITHOUT ANY WARRANTY; without even the implied warranty of      //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   //
// Lesser General Public License for more details.                     //
//                                                                     //
// You should have received a copy of the GNU Lesser General Public    //
// License along with this library; if not, write to the Free Software //
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       //
// 02110-1301  USA                                                     //
/////////////////////////////////////////////////////////////////////////
#include "omniwidget.h"
#include "pulseanimation.h"

#include <QPixmap>
#include <QPainter>

#include <QDebug>


QVariant OmniWidget::itemChange(GraphicsItemChange change, const QVariant &value)
{

    if (change == ItemZValueChange)
        qDebug() << "Child changed! Value:" << change << "\twidth: "
        << geometry().width();

    return QGraphicsItem::itemChange(change, value);
}

OmniWidget::OmniWidget( QGraphicsItem *parent )
    : QGraphicsWidget( parent ), pulser1( 0 ), pulser2( 0 )
{
    mLayout = new QGraphicsLinearLayout( Qt::Vertical, this);
    mLayout->setMinimumSize(0,0);

    addItem = new OmniItem(this);
    editItem = new OmniItem(this);
    deleteItem = new OmniItem(this);

    connect(addItem, SIGNAL(clicked()), this, SIGNAL(addItemClicked()));
    connect(editItem, SIGNAL(clicked()), this, SIGNAL(editItemClicked()));
    connect(deleteItem, SIGNAL(clicked()), this, SIGNAL(deleteItemClicked()));

    setOpacity(0.9);
    setGeometry(0,0,0,0);

    setMinimumSize(0,0);
    setFlag(QGraphicsItem::ItemIgnoresParentOpacity);
}

OmniWidget::~OmniWidget()
{
    delete addItem;
    delete editItem;
    delete deleteItem;
    delete pulser1;
    delete pulser2;
    delete pulser3;
}

void OmniWidget::createItems()
{
    addItem->setPixmapPath(mImagePath + "list-add-user.png");
    addItem->setText("Add Contact");

    editItem->setPixmapPath(mImagePath + "document-edit.png");
    editItem->setText("Edit Contact");

    deleteItem->setPixmapPath(mImagePath + "list-remove.png");
    deleteItem->setText("Delete Contact");

    mLayout->addItem(addItem);
    mLayout->addItem(editItem);
    mLayout->addItem(deleteItem);

    pulser1 = new Pulser<OmniItem>( editItem );
    pulser2 = new Pulser<OmniItem>( deleteItem );
    pulser3 = new Pulser<OmniItem>( addItem );
    connect( editItem, SIGNAL( clicked() ), pulser1, SLOT( start() ) );
    connect( deleteItem, SIGNAL( clicked() ), pulser2, SLOT( start() ) );
    connect( addItem, SIGNAL( clicked() ), pulser3, SLOT( start() ) );
}

void OmniWidget::updateItemPulser()
{
    if ( ( ( editItem->geometry().width() -
             pulser1->geometry().width() ) > 20 ) ||
         ( ( editItem->geometry().height() -
             pulser1->geometry().height() ) > 20 ) ) {

        qDebug() << "********************updating edit****************";
        pulser1->updateGeometry( editItem->geometry() );

    }

    if ( ( ( deleteItem->geometry().width() -
             pulser2->geometry().width() ) > 20 ) ||
         ( ( deleteItem->geometry().height() -
             pulser2->geometry().height() ) > 20 ) ) {
        qDebug() << "********************updating delete****************";
        pulser2->updateGeometry( deleteItem->geometry() );
    }

    if ( ( ( addItem->geometry().width() -
             pulser3->geometry().width() ) > 20 ) ||
         ( ( addItem->geometry().height() -
             pulser3->geometry().height() ) > 20 ) ) {
        qDebug() << "********************updating add****************";
        pulser3->updateGeometry( addItem->geometry() );
    }

}

void OmniWidget::paint( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget )
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setBrush(Qt::white);
    painter->setPen(Qt::white);
    painter->drawRoundedRect(rect(), 19, 19);

    /* Layout resized the item, so we do the same with the pulser */
    updateItemPulser();
}

void OmniWidget::setImagePath( const QString &path)
{
    mImagePath = path;
}
