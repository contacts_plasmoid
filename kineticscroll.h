#ifndef KINETICSCROLLING_H
#define KINETICSCROLLING_H

#include <QtCore/qglobal.h>
#include <QtCore/qmetatype.h>
#include <QGraphicsSceneMouseEvent>

#include "kineticscroll_p.h"

class KineticScrolling
{
    public:
        KineticScrolling();
    protected:
        void mouseMoveEvent( QGraphicsSceneMouseEvent *event);
        void mousePressEvent( QGraphicsSceneMouseEvent *event);
        void mouseReleaseEvent( QGraphicsSceneMouseEvent *event);

        qreal duration();
        qreal movement();

    private:
        KineticScrollingPrivate *d;
};

#endif
