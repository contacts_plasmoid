/***********************************************************************/
/* omniitem.h 							       */
/* 								       */
/* Copyright(C) 2009 Igor Trindade Oliveira <igor.oliveira@indt.org.br>*/
/* Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>*/
/* 								       */
/* This library is free software; you can redistribute it and/or       */
/* modify it under the terms of the GNU Lesser General Public	       */
/* License as published by the Free Software Foundation; either	       */
/* version 2.1 of the License, or (at your option) any later version.  */
/*   								       */
/* This library is distributed in the hope that it will be useful,     */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   */
/* Lesser General Public License for more details.		       */
/*  								       */
/* You should have received a copy of the GNU Lesser General Public    */
/* License along with this library; if not, write to the Free Software */
/* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       */
/* 02110-1301  USA						       */
/***********************************************************************/
#ifndef OMNIITEM_H
#define OMNIITEM_H

#include <QGraphicsWidget>
#include <QPixmap>

class OmniItem : public QGraphicsWidget
{
    Q_OBJECT

    public:
        OmniItem(QGraphicsItem *parent = 0);
	OmniItem(OmniItem &item);
        ~OmniItem();

        void paint ( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0 );
        void mousePressEvent ( QGraphicsSceneMouseEvent * event );

        void setPixmapPath( const QString &pixmapPath);
        void setText( const QString &text);

    Q_SIGNALS:
        void clicked();

    protected:
        void hoverEnterEvent ( QGraphicsSceneHoverEvent * event );
        void hoverLeaveEvent ( QGraphicsSceneHoverEvent * event );

    private:
        QString mText;
        QPixmap mPixmap;
        bool hasHoverEnter;
};

#endif
