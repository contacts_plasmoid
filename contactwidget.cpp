#include "contactwidget.h"
#include "contactlabel.h"
#include "contactphoto.h"

#include <QGraphicsGridLayout>

#include <kstandarddirs.h>

#ifndef STANDALONE
#include <kstandarddirs.h>
#endif

ContactWidget::ContactWidget( QGraphicsItem *parent )
    : QGraphicsWidget( parent )
{
    layout = new QGraphicsGridLayout( this );

    mPhoto = new ContactPhoto( this );
    label = new ContactLabel( this );

#ifndef STANDALONE
    mPhoto->setPhoto( KStandardDirs::installPath("data") + "plasma-contacts/" + "wallace-ld.jpg" );
    mPhoto->setShadow( KStandardDirs::installPath("data") + "plasma-contacts/" + "shadow.png" );
#else
    mPhoto->setPhoto( "./images/wallace-ld.jpg" );
    mPhoto->setShadow( "./images/shadow.png" );
#endif

    label->setText( "teste" );

    QPen pen;
    pen.setColor( QColor( Qt::black ) );
    label->setPen( pen );

    QFont font( "Helvetica [Cronyx]", 12 );
    label->setFont( font );

    layout->addItem( mPhoto, 0, 0 );
    layout->addItem( label, 0, 1 );
}

ContactWidget::~ContactWidget()
{
}
