
#include "contactphoto.h"
#include <QPixmap>
#include <QPainter>
#include <QGraphicsSceneEvent>
#include <QImage>
#include <math.h>
#include <QDebug>

static const double pi = 3.14159265359;


ContactPhoto::ContactPhoto(QGraphicsWidget *parent,
			   QGraphicsScene *scene): QGraphicsWidget( parent ),
						   grabbed( false ),
                                                   photo( 0 ), source( 0 ),
                                                   shadow( 0 ), result( 0 )
{
    QRectF tmp;
    if (parent)
	tmp = QRectF(10, 10, parent->geometry().width()/3,
		     parent->geometry().height()/3);
    else
	    tmp = QRectF(10, 10, 100, 200);

    setGeometry(tmp);

    setAcceptsHoverEvents(true);
    setFlag(ItemIgnoresParentOpacity);

}

void ContactPhoto::setPhoto(const QString &pixPath)
{
    if ( !source )
        source = new QImage;
    source->load( pixPath );

    if ( photo )
        delete photo;
    photo = new QPixmap( QPixmap::fromImage( *source ) );

}

ContactPhoto::~ContactPhoto()
{

    delete photo;
    delete source;
    delete shadow;
    delete result;

    qDebug() << "contactphoto result: " << result << "\n";
}

void ContactPhoto::setShadow(const QString &pixPath)
{
    QSize size(110, 110);

    if ( !shadow )
        shadow = new QImage;
    shadow->load( pixPath );

    qDebug() << "contactphoto  result: " << result;
    if ( !result )
        result = new QImage( size, QImage::Format_ARGB32_Premultiplied );
    qDebug() << "contactphoto result: " << result;

    createShadow();
}

void ContactPhoto::createShadow()
{

    QPainter painter( result );
    painter.setCompositionMode(QPainter::CompositionMode_Source);
    painter.fillRect(result->rect(), Qt::transparent);
    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.drawImage(-10, -10, *shadow);
    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
    painter.drawImage(0, 0, *source);
    painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);
    painter.fillRect(result->rect(), Qt::transparent);
    painter.end();

    if ( photo )
        delete photo;
    photo = new QPixmap( QPixmap::fromImage( *result ) );

}
void ContactPhoto::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
	   QWidget *widget)
{
	Q_UNUSED(widget);
	Q_UNUSED(option);
	painter->drawPixmap(QPoint(10, 10), *photo);
	/* TODO: make a round image */
}

QRectF ContactPhoto::boundingRect() const
{
    return QRectF(-100, -100, 200, 200);
}


/* TODO: cleanup this mess! */
/***************************************************************************/
static qreal lengthForPos(const QPointF &pos)
{
    return ::sqrt(pos.x() * pos.x() + pos.y() * pos.y());
}

static qreal angleForPos(const QPointF &pos)
{
    qreal angle = ::acos(pos.x() / lengthForPos(pos));
    if (pos.y() > 0)
        angle = pi * 2.0 - angle;
    return angle;
}

void ContactPhoto::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    return QGraphicsItem::mousePressEvent(event);
}

void ContactPhoto::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (!grabbed)
        return QGraphicsItem::mouseMoveEvent(event);

    /* TODO: use it for something useful */
    QPointF buttonDownPos = mapFromScene(event->buttonDownScenePos(Qt::LeftButton));

}

void ContactPhoto::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (!grabbed)
        return QGraphicsItem::mouseReleaseEvent(event);

    grabbed = false;
    update();
}


void ContactPhoto::setHover(qreal value)
{
    hover = value;
    update();
}
