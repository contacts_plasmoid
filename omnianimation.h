/*********************************************************************/
/* 								     */
/* Copyright (C)  2009  Adenilson Cavalcanti <>			     */
/* 								     */
/* This program is free software; you can redistribute it and/or     */
/* modify it under the terms of the GNU General Public License	     */
/* as published by the Free Software Foundation; either version 2    */
/* of the License, or (at your option) any later version.	     */
/*   								     */
/* This program is distributed in the hope that it will be useful,   */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of    */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     */
/* GNU General Public License for more details.			     */
/* 								     */
/* You should have received a copy of the GNU General Public License */
/* along with this program; if not, write to the Free Software	     */
/* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA     */
/* 02110-1301, USA.						     */
/*********************************************************************/

#ifndef OMNIANIMATION_H
#define OMNIANIMATION_H

#include <QObject>

class QGraphicsWidget;
class QPropertyAnimation;
class QSizeF;
class QParallelAnimationGroup;

class OmniAnimation {

public:
    OmniAnimation(QObject *parent, QGraphicsWidget *animatedObject, QObject *sender);

    ~OmniAnimation();

    void create( QSizeF *size, qreal widthLayoutMargin, qreal xWidgetMargin,
            qreal yWidgetMargin);

    void reset( QSizeF *size, qreal widthLayoutMargin, qreal xWidgetMargin,
            qreal yWidgetMargin );

protected:
        QPropertyAnimation *mOmniGeometryAnimOne;
        QPropertyAnimation *mOmniGeometryAnimTwo;
        QParallelAnimationGroup *parallelAnim;
        QParallelAnimationGroup *parallelAnimTwo;

private:
        QObject *mParent;
        QGraphicsWidget *mAnimatedObject;
        QObject *mSender;
};



#endif
