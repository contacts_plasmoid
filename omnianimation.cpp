#include "omnianimation.h"

#include <QGraphicsWidget>
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>

#include <QDebug>
OmniAnimation::OmniAnimation(QObject *parent, QGraphicsWidget *animatedObject, QObject *sender)
    : mParent( parent ), mAnimatedObject( animatedObject ), mSender( sender ),
    mOmniGeometryAnimOne( 0 ), mOmniGeometryAnimTwo( 0 ), parallelAnim( 0 ),
    parallelAnimTwo( 0 )
{

}

OmniAnimation::~OmniAnimation()
{
    delete mOmniGeometryAnimTwo;
    delete mOmniGeometryAnimOne;
    delete parallelAnim;
    delete parallelAnimTwo;
}

void OmniAnimation::create( QSizeF *size, qreal widthLayoutMargin,
                            qreal xWidgetMargin, qreal yWidgetMargin )
{

    // The states and animations.
/*( QObject * sender, const char * signal, QAbstractState * target ) */
//geometry animation
    mOmniGeometryAnimOne = new QPropertyAnimation(mAnimatedObject, "geometry");
    mOmniGeometryAnimOne->setEndValue(QRectF( (xWidgetMargin + widthLayoutMargin),
                size->height() - 360 - (yWidgetMargin + widthLayoutMargin) ,
                size->width() - (xWidgetMargin + widthLayoutMargin)*2, 360));
    mOmniGeometryAnimOne->setStartValue(QRectF( xWidgetMargin + widthLayoutMargin,
                size->height() - (yWidgetMargin + widthLayoutMargin),
                 size->width() - (xWidgetMargin + widthLayoutMargin)*2, size->height()));
    mOmniGeometryAnimOne->setDuration(600);
    mOmniGeometryAnimOne->setEasingCurve(QEasingCurve::InOutCubic);
//opacity animation
    QPropertyAnimation *paletteAnim = new QPropertyAnimation(mParent, "opacity");
    paletteAnim->setDuration(900);
    paletteAnim->setEasingCurve(QEasingCurve::InQuad);
    paletteAnim->setStartValue(qreal(1));
    paletteAnim->setEndValue(qreal(0.05));
//group animation
    parallelAnim = new QParallelAnimationGroup;
    parallelAnim->addAnimation(mOmniGeometryAnimOne);
    parallelAnim->addAnimation(paletteAnim);

    QObject::connect( mSender, SIGNAL( clicked() ), parallelAnim, SLOT( start() ) );
//new transition

//second geometry animation
    mOmniGeometryAnimTwo = new QPropertyAnimation(mAnimatedObject, "geometry");
    mOmniGeometryAnimTwo->setStartValue( mOmniGeometryAnimOne->endValue() );
    //mOmniGeometryAnimTwo->setEndValue( mOmniGeometryAnimOne->startValue() );
    mOmniGeometryAnimTwo->setEndValue( QRectF((xWidgetMargin + widthLayoutMargin), -(size->height() + 10), size->width() - (xWidgetMargin + widthLayoutMargin)*2, size->height()));
    mOmniGeometryAnimTwo->setEasingCurve( QEasingCurve::InCubic );
    mOmniGeometryAnimTwo->setDuration(400);
//second opacity animation
    QPropertyAnimation *secondOpacitty = new QPropertyAnimation(mParent, "opacity");
    secondOpacitty->setStartValue( qreal(0.05) );
    secondOpacitty->setEndValue( qreal(1) );
    secondOpacitty->setDuration(700);

    parallelAnimTwo = new QParallelAnimationGroup;
    parallelAnimTwo->addAnimation(mOmniGeometryAnimTwo);
    parallelAnimTwo->addAnimation(secondOpacitty);

    QObject::connect( mAnimatedObject, SIGNAL( addItemClicked() ), parallelAnimTwo, SLOT( start() ) );
}

void OmniAnimation::reset(QSizeF *size,
                          qreal widthLayoutMargin, qreal xWidgetMargin,
                          qreal yWidgetMargin)
{
     mOmniGeometryAnimOne->setEndValue(QRectF( (xWidgetMargin + widthLayoutMargin),
                 size->height() - 360 - (yWidgetMargin + widthLayoutMargin) ,
                 size->width() - (xWidgetMargin + widthLayoutMargin)*2, 360));

     mOmniGeometryAnimOne->setStartValue(QRectF( xWidgetMargin + widthLayoutMargin,
                 size->height() - (yWidgetMargin + widthLayoutMargin),
                 size->width() - (xWidgetMargin + widthLayoutMargin)*2, size->height()));

    qDebug()<<"OmniAnimation::reset:"<<size->width();

    mOmniGeometryAnimTwo->setStartValue( mOmniGeometryAnimOne->endValue() );
    mOmniGeometryAnimTwo->setEndValue( QRectF((xWidgetMargin + widthLayoutMargin),
                -(size->height() + 10), size->width() - (xWidgetMargin + widthLayoutMargin)*2, size->height()));
}
