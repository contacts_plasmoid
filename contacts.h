/***********************************************************************/
/* contacts.h 							       */
/* 								       */
/* Copyright(C) 2009 Igor Trindade Oliveira <igor.oliveira@indt.org.br>*/
/* Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>*/
/* 								       */
/* This library is free software; you can redistribute it and/or       */
/* modify it under the terms of the GNU Lesser General Public	       */
/* License as published by the Free Software Foundation; either	       */
/* version 2.1 of the License, or (at your option) any later version.  */
/*   								       */
/* This library is distributed in the hope that it will be useful,     */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   */
/* Lesser General Public License for more details.		       */
/*  								       */
/* You should have received a copy of the GNU Lesser General Public    */
/* License along with this library; if not, write to the Free Software */
/* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       */
/* 02110-1301  USA						       */
/***********************************************************************/
#ifndef FLIPPINEXAMPLE_H
#define FLIPPINEXAMPLE_H

#include <Plasma/PopupApplet>
#include <Plasma/DataEngine>
#include <Plasma/ScrollWidget>

#include <QtGui>

class QRectF;
class ContactButton;
class QtKineticListController;
class ContactWidget;

#include "omniwidget.h"
#include "omnianimation.h"
#include "pulser.h"

class ContactsPlasmoid : public Plasma::Applet
{
    Q_OBJECT

    public:
        ContactsPlasmoid(QObject *parent, const QVariantList &args);
        ~ContactsPlasmoid();
        void init();

    protected:
        QSizeF  sizeHint ( Qt::SizeHint which, const QSizeF & constraint = QSizeF() ) const;
        void resizeEvent( QGraphicsSceneResizeEvent * event );

    private:
        void animation();
        void rotateAnimation();
        void resetRotateAnimation();

    public Q_SLOTS:
        void makeVisible();
        void hideWidget();
        void backwardAnimation();
        void forwardAnimation();

    private:
        Plasma::ScrollWidget *mScrollWidget;
        OmniAnimation *mOmnianimation;
        QRectF *previousDim;
        QGraphicsWidget *mGraphicsWidget;
        QGraphicsLinearLayout *mLayout;
        QGraphicsLinearLayout *mGraphicsLayout;
        OmniWidget *mOmniWidget;
        ContactButton *mOmniButton;
        ContactButton *mHomeButton;
        QtKineticListController *controller;
        Pulser<ContactButton> *pulser;
        ContactWidget *mContactWidget;
        QPropertyAnimation *yRotationAnim;
        QPropertyAnimation *omniButtonAnim;
        QPropertyAnimation *homeButtonAnim;
        QPropertyAnimation *yWidgetRotationAnim;
        QSequentialAnimationGroup *seqAnimation;
};

#endif
