#include <qlistmodelinterface.h>
#include "contactsmodel.h"

ContactsModel::ContactsModel(QObject *parent )
    :QtListModelInterface( parent )
{
    names.append("teste foobar");
    names.append("teste2 foobar");
    names.append("teste3 foobar");
    names.append("teste4 foobar");
    names.append("teste5 foobar");
    names.append("teste6 foobar");
    names.append("teste7 foobar");
    names.append("teste8 foobar");
    names.append("teste9 foobar");
    names.append("teste10 foobar");
    names.append("teste11 foobar");
    names.append("teste12 foobar");
    names.append("teste13 foobar");
    names.append("teste14 foobar");
    names.append("teste15 foobar");

}

int ContactsModel::count() const
{
    return names.count();
}

QHash<int,QVariant> ContactsModel::data(int index, const QList<int> &roles) const
{
    QHash<int,QVariant> hash;

    for( int i =0 ; i < roles.count(); i++) {
        if( roles.at(i) == 1)
            hash.insert(1, names.at(index));
    }
    return hash;
}
