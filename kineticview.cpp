#include "kineticview.h"

#include <QObject>
#include <QDebug>

KineticView::KineticView( QGraphicsWidget *parent)
    : QGraphicsWidget( parent )
{
    setFiltersChildEvents ( true );
    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setAcceptedMouseButtons( Qt::LeftButton );
    setAcceptTouchEvents( true );

    scroll = new QPropertyAnimation(this, "y");

    resize(300, 700);
}

KineticView::~KineticView()
{
    //for(int i = 0; i < itemList.size(); i++)
    //    delete itemList.at(i);
    delete scroll;
}

void KineticView::scrollingAnimation(unsigned int duration, qreal startValue, qreal endValue)
{
    scroll->setEasingCurve( QEasingCurve::OutQuart );
    scroll->setDuration( duration*3 );
    scroll->setStartValue(startValue);
    scroll->setEndValue(endValue);
    scroll->start();
}

void KineticView::insertItem( QGraphicsWidget *item)
{
    item->setParentItem( this );
    item->setParentLayoutItem( this );

    if ( !itemList.empty() ) {
        QRectF tmpGeometry = itemList.last()->geometry();
        item->setGeometry( QRectF(tmpGeometry.x(), tmpGeometry.y() + tmpGeometry.height(), item->geometry().width(), item->geometry().height()));
    }
    itemList.append( item );

    update();
}

QGraphicsItem *KineticView::itemAt( int i )
{
    return itemList.at(i);
}

bool KineticView::event ( QEvent * event )
{
    switch( event->type() ) {
        case QEvent::GraphicsSceneMousePress:
            KineticScrolling::mousePressEvent(static_cast<QGraphicsSceneMouseEvent*>(event));
            QGraphicsWidget::mousePressEvent(static_cast<QGraphicsSceneMouseEvent*>(event));
            break;
        case QEvent::GraphicsSceneMouseMove:
            KineticScrolling::mouseMoveEvent(static_cast<QGraphicsSceneMouseEvent*>(event));
            QGraphicsWidget::mouseMoveEvent(static_cast<QGraphicsSceneMouseEvent*>(event));
            break;
        case QEvent::GraphicsSceneMouseRelease:
            KineticScrolling::mouseReleaseEvent(static_cast<QGraphicsSceneMouseEvent*>(event));

            if( movement() > 10) {
                QGraphicsWidget::mouseReleaseEvent(static_cast<QGraphicsSceneMouseEvent*>(event));
                scrollingAnimation(duration(), geometry().y(), geometry().y() - movement());
            }
            break;
        default:
            break;
    }

    return true;
}
