#ifndef __CONTACT_LABEL__
#define __CONTACT_LABEL__

#include <QGraphicsWidget>
#include <QPen>
#include <QRectF>
class QString;

class ContactLabel: public QGraphicsWidget
{
    Q_OBJECT
public:

  ContactLabel(QGraphicsItem *parent = 0);

  void setText(const QString &text);
  void setPen( const QPen &pen );
  void setFont( const QFont &font );

  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
	     QWidget *widget);


  //void mousePressEvent(QGraphicsSceneMouseEvent *event);
  //void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
  void setHover(qreal value);

  virtual ~ContactLabel();

protected:

  QString label;
  QPen mPen;
  QFont mFont;
  bool grabbed;
  qreal rotation;
  qreal newRotation;
  qreal scale;
  qreal newScale;
  qreal hover;
};


#endif
