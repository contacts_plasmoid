#ifndef CONTACTSVIEW_H
#define CONTACTSVIEW_H

#include <QStyleOptionViewItemV4>
#include <QGraphicsWidget>

#include "qgraphicslistview.h"

class ContactsView : public QtGraphicsListView
{
    Q_OBJECT

    public:
        ContactsView( QGraphicsWidget *parent = 0);
        void initStyleOption(QStyleOptionViewItemV4 *option) const;

    Q_SIGNALS:
        void clicked();

    protected:
        bool event(QEvent *event);
        void mousePressEvent( QGraphicsSceneMouseEvent *event );


};
#endif
