/*********************************************************************/
/* 								     */
/* Copyright (C)  2009  Adenilson Cavalcanti <>			     */
/* 								     */
/* This program is free software; you can redistribute it and/or     */
/* modify it under the terms of the GNU General Public License	     */
/* as published by the Free Software Foundation; either version 2    */
/* of the License, or (at your option) any later version.	     */
/*   								     */
/* This program is distributed in the hope that it will be useful,   */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of    */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the     */
/* GNU General Public License for more details.			     */
/* 								     */
/* You should have received a copy of the GNU General Public License */
/* along with this program; if not, write to the Free Software	     */
/* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA     */
/* 02110-1301, USA.						     */
/*********************************************************************/

#ifndef PULSER_H
#define PULSER_H

#include <QObject>
#include <QGraphicsWidget>

class QAbstractAnimation;
class QEvent;
class ProxyOpacitySetter;
class AItem;
class QPropertyAnimation;

class PulseBase: public QObject
{
Q_OBJECT

public:
    PulseBase() { };

    void updateGeometry(QRectF updated, qreal zCoordinate = 0, qreal scale = 1.5);

    QRectF geometry();

    ~PulseBase();

public Q_SLOTS:
    void start();
    void resetPulser();

protected:

    void createAnimation(qreal _duration, qreal _scale,
			 ProxyOpacitySetter *_setter);
    QAbstractAnimation *animation;
    QGraphicsWidget *under;
    QRectF *pulseGeometry;
    qreal zvalue, mscale;
    QPropertyAnimation *anim1, *anim2, *anim3;
};

template <typename TYPE>
class Pulser: public PulseBase
{

public:

    Pulser( TYPE *target, qreal scale = 1.5, qreal duration = 500,
	    ProxyOpacitySetter *setter = 0 )
    {
	under = new TYPE( *target );
	under->setParentItem( target );
	zvalue = target->zValue();
	--zvalue;
	under->setOpacity( 0 );
	under->setZValue( zvalue );
	mscale = under->scale();
	createAnimation(duration, scale, setter);
    }

};

#endif
