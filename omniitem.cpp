/////////////////////////////////////////////////////////////////////////
// omniitem.cpp                                                        //
//                                                                     //
// Copyright(C) 2009 Igor Trindade Oliveira <igor.oliveira@indt.org.br>//
// Copyright(C) 2009 Adenilson Cavalcanti <adenilson.silva@idnt.org.br>//
//                                                                     //
// This library is free software; you can redistribute it and/or       //
// modify it under the terms of the GNU Lesser General Public          //
// License as published by the Free Software Foundation; either        //
// version 2.1 of the License, or (at your option) any later version.  //
//                                                                     //
// This library is distributed in the hope that it will be useful,     //
// but WITHOUT ANY WARRANTY; without even the implied warranty of      //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU   //
// Lesser General Public License for more details.                     //
//                                                                     //
// You should have received a copy of the GNU Lesser General Public    //
// License along with this library; if not, write to the Free Software //
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA       //
// 02110-1301  USA                                                     //
/////////////////////////////////////////////////////////////////////////
#include "omniitem.h"

#include <QDebug>
#include <QPainter>

OmniItem::OmniItem( QGraphicsItem *parent)
     : QGraphicsWidget(parent)
{
    setAcceptedMouseButtons(Qt::LeftButton);
    setAcceptHoverEvents(true);
    setMinimumSize(0,0);
    setFlag(QGraphicsItem::ItemClipsToShape, true);
}

OmniItem::OmniItem(OmniItem &item)
{
    setMinimumSize( 0,0 );
    setFlag( QGraphicsItem::ItemClipsToShape, true );
    setFlag( ItemIgnoresParentOpacity );
    setGeometry( item.geometry() );
    setPos( 0, 0 );
    mText = item.mText;
    mPixmap = item.mPixmap;
    hasHoverEnter = true;
}

OmniItem::~OmniItem()
{
}

void OmniItem::setPixmapPath( const QString &pixmapPath)
{
    if ( !mPixmap.load( pixmapPath ) )
        qDebug("error loading pixmap");
}

void OmniItem::setText( const QString &text)
{
    mText = text;
}

void OmniItem::paint ( QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{

    Q_UNUSED(option);
    Q_UNUSED(widget);

    QPen pen;
    pen.setCapStyle( Qt::FlatCap );
    pen.setWidth(2);
    if ( hasHoverEnter ) {
        pen.setColor(Qt::darkGray);
        painter->setPen( pen );
        painter->setBrush( Qt::white );
        painter->drawRoundedRect(QRectF(rect().x(), rect().y(), rect().width(), rect().height() - 10), 20, 20);

    } else {
        pen.setColor(Qt::lightGray);
        painter->setPen( pen );
        painter->drawRoundedRect(QRectF(rect().x(), rect().y(), rect().width(), rect().height() - 10), 20, 20);

    }

    QRectF geometryTemporary;
    if (!geometry().width())
        return;

    geometryTemporary.setX(mPixmap.width()+20);
    geometryTemporary.setY(mPixmap.height()/2);
    geometryTemporary.setWidth(130);
    geometryTemporary.setHeight(40);
    painter->drawPixmap(mPixmap.rect(), mPixmap);

    pen.setColor(Qt::black);
    painter->setPen( pen );
    painter->drawText(geometryTemporary, Qt::AlignCenter, mText);

}

void OmniItem::mousePressEvent ( QGraphicsSceneMouseEvent * event )
{
    Q_UNUSED(event);
    emit clicked();
}

void OmniItem::hoverEnterEvent ( QGraphicsSceneHoverEvent * event )
{
    Q_UNUSED(event);
    hasHoverEnter = true;
    update();
}

void OmniItem::hoverLeaveEvent ( QGraphicsSceneHoverEvent * event )
{
    Q_UNUSED(event);
    hasHoverEnter = false;
    update();
}
